window.jsonFUNC = function(){
    $('.load').fadeIn('slow');

    $.getJSON( window.userjson, function( data ) {
        $('#dieta-tab,#dieta-content,#dieta-relatorio').html('');
        window.json = data;
        JsonRequired();
        window.preencher_form();
        functions();
        dieta_atual();
        $('.load').fadeOut('slow');
    }).fail(function() {
        $.removeCookie("nome");
        $.removeCookie("email");
        $.removeCookie("uid");
        window.location.replace("index.html");
    });
};
window.jsonFUNC();

$(window).load(function () {

    $('#dieta-tab').tabCollapse();
    $('select').keypress(function (e) {
        if(e.which == 8 || e.which == 46 ) {
            $(this).find('option:selected').remove();
        }
    });
    var categorias = [];
    $.getJSON( "/framework/json/categorias.json", function( data ) {
        categorias = data["content"];
        console.log(categorias);
        $( ".autocomplete" ).autocomplete({
            source: categorias,
            select: function( event, ui ) {}
        })
        .on("autocompleteselect", function (event, ui) {
            $(this).parent().parent().find("select").append('<option value="' + ui.item.value + '">' + ui.item.value + '</option>');
            relatorio();
        })
        .on("autocompleteclose", function (event, ui) {
            $(this).val('');
            relatorio();
        });
    });

    $('input[name="treina"]').change(function () {
        if($('input[name="treina"]:checked').val() == "Não"){
            $('#trei-ref').fadeOut('slow');
        }else{
            $('#trei-ref').fadeIn('slow');
        }
    });

    $('#dieta-enviar').click(function () {
        $(this).val('Carregando...');
        var thisS = $(this);
        var post = 'uid='+ $.cookie('uid') +'&email='+ $.cookie('email');
        $('form').each(function () {
            post = post+'&'+$(this).serialize();
        });
        post = post+'&SomaKcal='+window.somaKcal+'&proteinas='+window.proteinas+'&carboidratos='+window.carboidratos+'&gorduras='+window.gorduras;
        post = post+'&alimentos_proibidos='+window.alimentos_proibidos+'&alimentos_preferidos='+window.alimentos_preferidos;
        post = post+'&TMB='+window.TMB;

        var link = window.link+'api/diet/act-ca-dieta.php';
        $('#login-img').fadeIn('slow');
        $.ajax({
            type: 'post',
            data: post,
            url: link,
            success: function (retorno) {
                if(retorno.reply.code == 1){
                    window.location.reload(true);
                }
                if(retorno.reply.code == 6){
                    alert('Você precisa assinar um de nossos planos para isso.');
                }
                thisS.val('Enviar');
            }
        });
    });

    $('input, select').on('keyup change', function () {
        functions();

        switch ($(this).attr('id')){
            case 'tmb-alt':
                window.TMB = parseFloat($(this).val());
            break;
            case 'cal-dia-alt':
                window.somaKcal = parseFloat($(this).val());
            break;
            case 'proteinas-alt':
                window.proteinas = parseFloat($(this).val());
            break;
            case 'carboidratos-alt':
                window.carboidratos = parseFloat($(this).val());
            break;
            case 'gorduras-alt':
                window.gorduras = parseFloat($(this).val());
            break;
        }
        relatorio();
        graficoNovaDieta();
    });
    $('#peso-number').on('change keyup',function () {
        if ( $.trim($('#peso-number').val()).length > 0 ){
            $('#atualizar-peso').prop('disabled',false);
        }else{
            $('#atualizar-peso').prop('disabled',true);
        }
    });
    $('#atualizar-peso').on('click', function () {
        $('#atualizar-peso').html('Carregando...');
        if($('#ajustar-dieta').is(':checked')){
            var data_ajax = 'uid='+ $.cookie('uid') + '&email=' + $.cookie('email') + '&peso=' + $('#peso-number').val() + '&ajustar=1';
        }else{
            var data_ajax = 'uid='+ $.cookie('uid') + '&email=' + $.cookie('email') + '&peso=' + $('#peso-number').val();
        }
        $.ajax({
            type: 'post',
            data: data_ajax,
            url: window.link + 'api/user/act-peso.php',
            success: function (retorno) {
                $('#atualizar-peso').html('Atualizar');
                if(retorno.reply.code == 1){
                    $('#peso-number').attr(
                        'placeholder',
                        $('#peso-number').val()
                    ).val('');
                    $('#footer-atualizar').append('<p style="font-size: smaller">Peso atualizado com sucesso.</p>');
                    location.reload();
                }
            }
        });
    });
    $('input[name="dieta-tipo"]').change(function () {
       if($(this).val() == "1"){
           $('#dieta-avancado').fadeOut('slow', function () {
               $('#dieta-simples').fadeIn('slow');
           });
       }else{
           $('#dieta-simples').fadeOut('slow', function () {
               $('#dieta-avancado').fadeIn('slow');
           });
       }
    });

});
function functions(){
    calcularTMB();
    getMacros();
    altMacros();
    relatorio();
    graficoNovaDieta();
    if('somaKcal' in window){
        $('#d-cal').html(window.somaKcal);
        $('#cal-dia-alt').attr('placeholder', window.somaKcal);
    }
    if(window.proteinas > 0 && window.carboidratos > 0 && window.gorduras > 0 && window.somaKcal > 0){
        $('#dieta-enviar').prop( "disabled", false );
    }else{
        $('#dieta-enviar').prop( "disabled", true );
    }
}

function calcularTMB(){
    window.peso    =  $('input[name="peso"]')   .val();
    window.idade   =  $('input[name="idade"]')  .val();
    window.altura  =  $('input[name="altura"]') .val();
    window.FA      =  $('#FA option:selected')  .val();

    if(window.FA > 0 && window.peso > 0 && window.idade > 0 && window.altura > 0) {
        if($('#tmb-alt').length > 0 && $('#tmb-alt').val().length == 0){
            if ($('input[name="sexo"]:checked').attr('id') == 'M') {
                window.TMB = window.FA * ( 66.47 + (13.75 * window.peso) + (5.00 * window.altura) - (6.80 * window.idade) );
            } else {
                window.TMB = window.FA * ( 655.1 + (09.56 * window.peso) + (1.85 * window.altura) - (4.68 * window.idade) );
            }
        }else{
            window.TMB = parseInt($(this).val());
        }
    }

    window.TMB = Math.round(window.TMB);
    $('#tmb').html(window.TMB);
    $('#tmb-alt').attr( 'placeholder' , window.TMB);
}
function getMacros(){
        switch($('input[name="objetivo"]:checked').val()){
            case 'bulk':
                if($('#proteinas-alt').val().length == 0 ) {
                    window.proteinas = 30;
                }else{
                    window.proteinas = parseInt($('#proteinas-alt').val());
                }
                if($('#gorduras-alt').val().length == 0 ) {
                    window.gorduras = 20;
                }else{
                    window.gorduras = parseInt($('#gorduras-alt').val());
                }
                if($('#carboidratos-alt').val().length == 0 ){
                    window.carboidratos = 50;
                }else{
                    window.carboidratos = parseInt($('#carboidratos-alt').val());
                }
                window.somaKcal     = parseInt(window.TMB+500);
            break;
            case 'cut':
                if($('#proteinas-alt').val().length == 0 ) {
                    window.proteinas = 40;
                }else{
                    window.proteinas = parseInt($('#proteinas-alt').val());
                }
                if($('#gorduras-alt').val().length == 0 ) {
                    window.gorduras = 25;
                }else{
                    window.gorduras = parseInt($('#gorduras-alt').val());
                }
                if($('#carboidratos-alt').val().length == 0 ){
                    window.carboidratos = 35;
                }else{
                    window.carboidratos = parseInt($('#carboidratos-alt').val());
                }
                window.somaKcal     = parseInt(window.TMB-500);
            break;
            case 'manter':
                if($('#proteinas-alt').val().length == 0 ) {
                    window.proteinas = 30;
                }else{
                    window.proteinas = parseInt($('#proteinas-alt').val());
                }
                if($('#gorduras-alt').val().length == 0 ) {
                    window.gorduras = 20;
                }else{
                    window.gorduras = parseInt($('#gorduras-alt').val());
                }
                if($('#carboidratos-alt').val().length == 0 ){
                    window.carboidratos = 50;
                }else{
                    window.carboidratos = parseInt($('#carboidratos-alt').val());
                }
                window.somaKcal     = parseInt(window.TMB);
            break;
        }
}
function altMacros(){
    if($('input[name="tipo-alt"]:checked').val() != '%'){

        var peso = parseFloat($('#peso').val());

        if(window.proteinas    > 0 && $('#proteinas-alt')    .val().length == 0){
            window.proteinas = ((window.somaKcal * (window.proteinas / 100) )/4) / peso;
            $('#proteinas-alt').attr( 'placeholder' , parseFloat(window.proteinas).toFixed(2) );
        }
        if(window.gorduras     > 0 && $('#gorduras-alt')     .val().length == 0){
            window.gorduras = (((window.somaKcal*(window.gorduras/100))/9)/peso);
            $('#gorduras-alt').attr( 'placeholder' , parseFloat(window.gorduras).toFixed(2) );
        }
        if(window.carboidratos > 0 && $('#carboidratos-alt') .val().length == 0){
            window.carboidratos = (((window.somaKcal*(window.carboidratos/100))/4)/peso);
            $('#carboidratos-alt').attr( 'placeholder' , parseFloat(window.carboidratos).toFixed(2) );
        }

    }else{
        if(window.proteinas == 0){ getMacros(); }
        $('#gorduras-alt')     .attr( 'placeholder' , window.gorduras );
        $('#proteinas-alt')    .attr( 'placeholder' , window.proteinas );
        $('#carboidratos-alt') .attr( 'placeholder' , window.carboidratos );
    }
}
function relatorio(){
    $('#ul_alimentos_preferidos').html('');
    window.alimentos_preferidos = '';
    window.alimentos_proibidos = '';

    var tipo = ' '+$('input[name="tipo-alt"]:checked').val();
    if('TMB' in window){
        $('#tmb-alt').attr('placeholder', Math.round(window.TMB));
        $('#rel-tmb').html('Taxa Metabolica Basal: <b>' + Math.round(window.TMB) + ' calorias.</b>');
    }
    if('somaKcal'     in window){
        $('#cal-dia-alt').attr('placeholder',window.somaKcal);
        $('#rel-calday').html('Calorias / Dia: <b>' + parseInt(window.somaKcal) + ' calorias.</b>');
    }
    if('gorduras'     in window){
        $('#gorduras-alt').attr('placeholder',window.gorduras);
    }
    if('proteinas'    in window){
        $('#proteinas-alt').attr('placeholder',window.proteinas);
    }
    if('carboidratos' in window){
        $('#carboidratos-alt').attr('placeholder',window.carboidratos);
    }

    var rel_obj = '';
    switch($('input[name="objetivo"]:checked').val()){
        case 'manter':
            rel_obj = 'Manter peso.';
        break;
        case 'bulk':
            rel_obj = 'Ganhar peso.';
        break;
        case 'cut':
            rel_obj = 'Emagrescer.';
        break;
    }

    $('#rel-objetivo').html('Objetivo: <b>'+ rel_obj +'</b>');
    $('#ul_alimentos_preferidos, #ul_alimentos_proibidos').html('');
    $('#alimentos-preferidos option') .each(function () {
        $('#ul_alimentos_preferidos').append('<li>'+$(this).val()+'</li>');
        if(!(window.alimentos_preferidos.indexOf($(this).val()) >=0 )){
            window.alimentos_preferidos = window.alimentos_preferidos+';'+$(this).val();
        }
    });
    $('#alimentos-proibidos option')  .each(function () {
        $('#ul_alimentos_proibidos').append('<li>'+$(this).val()+'</li>');
        if(!(window.alimentos_proibidos.indexOf($(this).val()) >=0 )) {
            window.alimentos_proibidos = window.alimentos_proibidos + ';' + $(this).val();
        }
    });
}
function graficoNovaDieta(){
    if(window.proteinas > 0 && window.carboidratos > 0 && window.gorduras > 0 && $('input[name="tipo-alt"]:checked').val() == '%'){
        $('#chartnewdiet').fadeIn('slow');
        var dataPreferences = {
            series: [
                [25, 30, 20, 25]
            ]
        };

        var optionsPreferences = {
            donut: true,
            donutWidth: 40,
            startAngle: 0,
            total: 100,
            showLabel: false,
            axisX: {
                showGrid: false
            }
        };

        Chartist.Pie('#chartnewdiet', dataPreferences, optionsPreferences);

        Chartist.Pie('#chartnewdiet', {
            labels: [window.carboidratos + '%', window.proteinas + '%', window.gorduras + '%'],
            series: [window.carboidratos, window.proteinas, window.gorduras]
        });
    }else{
        $('#chartnewdiet').fadeOut('slow');
    }
}

function dieta_atual(){
    if ('dieta' in window.json){
        $('#rowdieta').fadeIn('slow');
        $('#ajustar-peso').fadeIn('slow');
        $.each(window.json.perfil.peso.slice(-1).pop(), function (k, v) {
            $('#peso-number').attr('placeholder',k);
        });
        $('.dietajson').fadeIn('slow');
        console.log(window.json.dieta);

        var dieta    = window.json.dieta[(window.json.dieta.length)-1];
        var fator_multiplicacao = {
            gorduras     : 9 ,
            proteinas    : 4 ,
            carboidratos : 4
        };
        var dieta_meta = {
            macros : {
                gramas :{
                    gorduras     : (dieta.SomaKcal * (dieta.gorduras     / 100)) / 9 ,
                    proteinas    : (dieta.SomaKcal * (dieta.proteinas    / 100)) / 4 ,
                    carboidratos : (dieta.SomaKcal * (dieta.carboidratos / 100)) / 4
                },
                calorias :{
                    gorduras     : dieta.SomaKcal * (dieta.gorduras     / 100),
                    proteinas    : dieta.SomaKcal * (dieta.proteinas    / 100),
                    carboidratos : dieta.SomaKcal * (dieta.carboidratos / 100),
                    total : dieta.SomaKcal
                }
            }
        };
        console.log(dieta_meta);
        var dieta_real = {
            macros : {
                gramas :{
                    gorduras     : 0 ,
                    proteinas    : 0 ,
                    carboidratos : 0
                },
                calorias :{
                    gorduras     : 0 ,
                    proteinas    : 0 ,
                    carboidratos : 0 ,
                    total : 0
                }
            }
        };
        var data = dieta.data.split('-');
        data = data[2] + '/' + data[1] + '/' + data[0];

        switch(dieta.objetivo){
            case 'cut':
                var objetivo = 'Emagrescer (Cutting)';
                break;
            case 'bulk':
                var objetivo = 'Ganhar Peso (Bulking)';
                break;
            case 'manter':
                var objetivo = 'Manter Peso';
            break;
        }

        $('#data-dieta').html('<i class="fa fa-clock-o"></i> Criada em ' + data);
        $('#dieta-objetivo').html('<i class="fa fa-cutlery"></i> Objetivo: '+ objetivo);

        $.each(dieta.refeicao, function (c,d) {
            $.each(dieta.refeicao[c].macros.real.gramas, function (e,f) {
                dieta_real.macros.gramas[e]      += parseFloat(f);
                dieta_real.macros.calorias[e]    += parseFloat(f) * fator_multiplicacao[e];
                dieta_real.macros.calorias.total += parseFloat(f) * fator_multiplicacao[e];
            });
        });

        $.each(dieta.refeicao, function (ref,bb) {
            var active = '';
            if (ref == 1) { active = 'active'; } else { active = ''; }

            $('#dieta-tab').append(
                '<li class="' + active + '">'                      +
                    '<a href="#' + ref + '" data-toggle="tab">'    +
                        'Refeição '+ ref                           +
                    '</a>'                                         +
                '</li>'
            );

            $('#dieta-content')
                .append(
                '<div class="dietaRefeicao tab-pane ' + active + ' tabsDieta" id="' + ref + '">'              +
                    '<table class="table table-responsive table-striped" id="dieta-table">'     +
                        '<thead>'                                                               +
                            '<th> Alimento     </th>'                                           +
                            '<th class="removerMobile" style="text-align: center;"> Proteinas    </th>'               +
                            '<th class="removerMobile" style="text-align: center;"> Carboidratos </th>'               +
                            '<th class="removerMobile" style="text-align: center;"> Gorduras     </th>'               +
                            '<th style="text-align: center;"> Calorias     </th>'               +
                            '<th style="text-align: center;">Quantidade</th>'     +
                            '<th style="text-align: center;">Editar</th>'    +
                            '<th style="text-align: center"> Remover </th>'       +
                        '</thead>'                                                              +
                        '<tbody id="tr-'+ref+'">'                                               +
                        '</tbody>'                                                              +
                    '</table>'+
                    '<table class="table table-responsive table-striped table-ref" id="table-ref-'+ ref +'">' +
                        '<thead>'                                                                             +
                            '<th></th>'                                                                       +
                            '<th class="removerMobile" style="text-align: center;"> Esta Refeicao </th>'                            +
                            '<th class="removerMobile" style="text-align: center;"> Objetivo      </th>'                            +
                            '<th style="text-align: center;"> Total</th>'                     +
                            '<th style="text-align: center;"> Objetivo</th>'              +
                        '</thead>'                                                                            +
                        '<tbody>'                                                                             +
                        '</tbody>'                                                                            +
                    '</table>'                                                                                +
                '</div>'
            );
            $.each(dieta.refeicao[ref].alimentos, function (a,b) {
                var link = {
                    editar :{
                        rel : 'facebox', href : ''
                    },
                    remover :{
                        rel : 'delete-facebox', href : ''
                    },
                    detalhes:{
                        rel: 'detail-facebox', href: ''
                    }
                };
                var link_editar   = ' rel="' + link.editar.rel + '"   href="'+ link.editar.href   +'" class="'+ b.uid + '-xqnt-' + b.quantidade + '-xqnt-' + ref +'"';
                var link_remover  = ' rel="' + link.remover.rel + '"  href="'+ link.remover.href  +'" class="'+ b.uid + '-xqnt-' + b.quantidade + '-xqnt-' + ref +'"';
                var link_detalhes = ' rel="' + link.detalhes.rel + '" href="'+ link.detalhes.href +'" class="'+ b.uid + '-xqnt-' + b.quantidade +'"';

                $('#tr-' + ref).append(
                    '<tr class="alimento" rel="'+ ref +'">'                                             +
                        '<td>'                                                                          +
                            '<a' + link_detalhes + '>'+
                                b.nome                                                                  +
                            '</a>'                                                                      +
                        '</td>'                                                                         +
                        '<td class="removerMobile" style="text-align: center;">'  + Math.round(b.proteinas)    + ' g </td>'   +
                        '<td class="removerMobile" style="text-align: center;">'  + Math.round(b.carboidratos) + ' g </td>'   +
                        '<td class="removerMobile" style="text-align: center;">'  + Math.round(b.gorduras)     + ' g </td>'   +
                        '<td style="text-align: center;">'  + Math.round((b.gorduras*9)+(b.carboidratos*4)+(b.proteinas*4)) + ' kcal </td>'   +
                        '<td style="text-align: center;" class="green">' + b.quantidade  + ' g </td>'   +
                        '<td style="text-align: center;">'                                              +
                            '<a style="text-decoration:none;"' + link_editar + '>'  +
                                '<i class="fa fa-exchange"></i>'                                        +
                            '</a>'                                                                      +
                        '</td>'                                                                         +
                        '<td style="text-align: center;">'                                              +
                            '<a' + link_remover + ' style="color:darkred">'                             +
                                '<i class="fa fa-times"></i>'                                           +
                            '</a>'                                                                      +
                        '</td>'                                                                         +
                    '</tr>'
                );
            });

            $.each(dieta.refeicao[ref].macros.real.gramas, function (macro,quantidade) {
                if(dieta_real.macros.gramas[macro] < dieta_meta.macros.gramas[macro]){
                    color = 'darkred';
                } else if(dieta_real.macros.gramas[macro] > dieta_meta.macros.gramas[macro]) {
                    color = 'darkgreen';
                } else{
                    color = '#ccc';
                }

                $('#table-ref-'+ref+' tbody').append(
                    '<tr>'                                                                                    +
                        '<td>'                                                                                +
                            ucwords(macro) + ':  '                                                            +
                        '</td>'                                                                               +
                        '<td class="removerMobile" style="text-align: center" class="dblue">'                                       +
                            Math.round(quantidade) + ' g '                                                    +
                        '</td>'                                                                               +
                        '<td class="removerMobile" style="text-align: center">'                                                     +
                            Math.round(dieta_meta.macros.gramas[macro]/window.json.dieta.length) + ' g '      +
                        '</td>'                                                                               +
                        '<td style="text-align:center;color: '+ color +'">'                                   +
                            Math.round(dieta_real.macros.gramas[macro]) + ' g '                               +
                        '</td>'                                                                               +
                        '<td style="text-align: center;color: darkslateblue">'                                +
                            Math.round(dieta_meta.macros.gramas[macro]) + ' g '                               +
                        '</td>'                                                                               +
                    '</tr>'
                );
            });

            var color = '#ccc';

            if(dieta_real.macros.calorias.total < dieta_meta.macros.calorias.total){
                color = 'darkred';
            } else {
                color = 'darkgreen';
            }

            $('#table-ref-'+ref+' tbody').append(
                '<tr>'                                                                                    +
                    '<td>Calorias:</td>'                                                                  +
                    '<td class="removerMobile" style="text-align: center" class="dblue">'                                                      +
                        Math.round(dieta.refeicao[ref].calorias) + ' kcal '                               +
                    '</td>'                                                                               +
                    '<td class="removerMobile" style="text-align: center">'                                                                    +
                        Math.round(dieta_meta.macros.calorias.total/window.json.dieta.length)  + ' kcal ' +
                    '</td>'                                                                               +
                    '<td style="text-align:center;color: '+ color +'">'                                   +
                        Math.round(dieta_real.macros.calorias.total) + ' kcal '                           +
                    '</td>'                                                                               +
                    '<td style="text-align: center;color: darkslateblue">'                                +
                        Math.round(dieta_meta.macros.calorias.total) + ' kcal '                           +
                    '</td>'                                                                               +
                '</tr>'
            );

            $('#tr-' + ref).append(
                '<tr>' +
                    '<td>' +
                        '<a rel="faceboxnew" class="link-'+ ref +'">' +
                            'Adicionar Alimento' +
                        '</a>' +
                    '</td>' +
                    '<td class="removerMobile"></td>' +
                    '<td class="removerMobile"></td>' +
                    '<td class="removerMobile"></td>' +
                    '<td class="removerMobile"></td>' +
                    '<td class="removerMobile"></td>' +
                    '<td class="removerMobile"></td>' +
                    '<td class="removerMobile"></td>' +
                '</tr>'
            );
        });

        var grafico = {
            gorduras     : Math.round((dieta_real.macros.calorias.gorduras     / dieta_real.macros.calorias.total) * 100),
            proteinas    : Math.round((dieta_real.macros.calorias.proteinas    / dieta_real.macros.calorias.total) * 100),
            carboidratos : Math.round((dieta_real.macros.calorias.carboidratos / dieta_real.macros.calorias.total) * 100)
        };

        console.log(dieta);

        grafico_dieta_atual(grafico);
        link_facebox();

    }
}
function grafico_dieta_atual(grafico){
    var dataPreferences = {
        series: [
            [25, 30, 20, 25]
        ]
    };
    var optionsPreferences = {
        donut: true,
        donutWidth: 40,
        startAngle: 0,
        total: 100,
        showLabel: false,
        axisX: {
            showGrid: false
        }
    };

    Chartist.Pie('#chartPreferences', dataPreferences, optionsPreferences);
    Chartist.Pie('#chartPreferences', {
        labels: [grafico.carboidratos+'%', grafico.proteinas+'%', grafico.gorduras+'%'],
        series: [grafico.carboidratos    , grafico.proteinas    , grafico.gorduras]
    });
}
function link_facebox(){
    $('a[rel="faceboxnew"]').click(function (event) {
        /*event.preventDefault();*/
        var atributos = $(this).attr('class').substr(5);
        window.ref    = atributos;
        window.adicionar_acao = 'dieta';

        $.get('template/adicionar-alimento.html', function (data) {
            bootbox.alert({
                message: data,
                title: '<h4 id="alm-nome"></h4>'
            });
        });
    });

    $('a[rel="facebox"]').click(function (event) {
        event.preventDefault();
        var atributos = $(this).attr('class').split('-xqnt-');
        window.editar_acao = 'dieta';
        window.ref     = atributos[2];
        window.alm_qnt = atributos[1];
        window.alm_uid = atributos[0];
        $.get('template/editar-alimento.html', function (data) {
            bootbox.alert({
                message: data,
                title: '<h4 id="alm-nome"></h4>'
            });
        });
    });

    $('a[rel="detail-facebox"]').click(function (event) {
        event.preventDefault();
        var atributos = $(this).attr('class').split('-xqnt-');
        window.editar_acao = 'dieta';
        window.ref     = atributos[2];
        window.alm_qnt = atributos[1];
        window.alm_uid = atributos[0];
        $.get('template/alimentos.html', function (data) {
            bootbox.alert({
                message: data,
                title: '<h4 id="alm-nome"></h4>'
            });
        });
    });

    $('a[rel="delete-facebox"]').click(function (event) {
        event.preventDefault();
        var atributos = $(this).attr('class').split('-xqnt-');
        window.editar_acao = 'dieta';
        window.ref     = atributos[2];
        window.alm_qnt = atributos[1];
        window.alm_uid = atributos[0];

        $.get('template/remover-alimento.html', function (data) {
            bootbox.alert({ 
                size: 'small',
                message: data,
                callback: function(result){}
            })
        });
    });
}
