$(function () {
    $('#tabs').tabCollapse();

    $.getjson();
    adicionarTreino();
    RemoverTreino();

    editarNome();
    editarCategoria();
    editarProgresso();
    editarObservacoes();

    listarExercicios();
    addNovoExercicio();
    apagarExercicios();

    trocarOrganizacaoTreino();

    window.categorias = {
        Peso:        ['Kg','G'],
        Resistencia: ['Km','m'],
        Velocidade:  ['Km/h','m/s'],
        Outro: ['']
    };
    window.dias = ['Segunda-Feira','Terca-Feira','Quarta-Feira','Quinta-Feira','Sexta-Feira','Sábado','Domingo'];
    window.letras = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    console.log();
});

$.getjson = function(){
    $.getJSON( window.userjson, function( data ) {
        window.json = data;
        console.log(data);
        JsonRequired();
    }).fail(function() {
        $.removeCookie("nome");
        $.removeCookie("email");
        $.removeCookie("uid");
        window.location.replace("index.html");
    });
};


function editarNome(){
    $('#my-tab-content').on('click','.editarnome', function () {
        var texto = $(this).text();
        $(this).parent().html('<input type="text" class="editarnomeInput form-control" value="'+texto+'">');
    });
    $('#my-tab-content').on('change','.editarnomeInput', function () {
        var thisS = $(this);
        var id = $(this).parent().parent().attr('id');
        var treino = $(this).parent().parent().parent().parent().parent().parent().attr('id');
        var texto = $(this).val();

        thisS.after('<i class="fa fa-spinner fa-pulse"></i>');

        var data_enviar =
            'uid=' + $.cookie('uid') +
            '&email=' + $.cookie('email') +
            '&treino=' + treino +
            '&ex_id=' + id +
            '&ex_nome=' + texto;

        $.ajax({
            type: 'post',
            data: data_enviar,
            url: window.link + 'api/exercicio/act-editarnome-exercicio.php',
            success: function (retorno) {
                console.log(retorno);
                if (retorno.reply.code == 1) {
                    thisS.parent().html('<a href="#" class="editarnome">' + texto + '</a>');
                }
                if(retorno.reply.code == 6){
                    alert('Você precisa assinar um de nossos planos para isso.')
                }
            }
        });
    });
}
function editarCategoria(){

    $('#my-tab-content').on('change','.categoria', function () {
        var thisS = $(this);
        var id = $(this).parent().parent().attr('id');
        var treino = $(this).parent().parent().parent().parent().parent().parent().attr('id');
        var texto = $(this).val();

        thisS.after('<i class="fa fa-spinner fa-pulse"></i>');

        var data_enviar =
            'uid=' + $.cookie('uid') +
            '&email=' + $.cookie('email') +
            '&treino=' + treino +
            '&ex_id=' + id +
            '&ex_cat=' + texto;

        $.ajax({
            type: 'post',
            data: data_enviar,
            url: window.link + 'api/exercicio/act-editarcategoria-exercicio.php',
            success: function (retorno) {
                console.log(retorno);
                thisS.parent().children('i').remove();
            }
        });
    });
}
function editarProgresso(){

    $('#my-tab-content').on('change','.editarProgressao', function () {
        var thisS = $(this);
        var id = $(this).parent().parent().attr('id');
        var treino = $(this).parent().parent().parent().parent().parent().parent().attr('id');
        var texto = $(this).val();

        thisS.after('<i class="fa fa-spinner fa-pulse"></i>');

        var data_enviar =
            'uid=' + $.cookie('uid') +
            '&email=' + $.cookie('email') +
            '&treino=' + treino +
            '&ex_id=' + id +
            '&ex_progresso=' + texto;

        $.ajax({
            type: 'post',
            data: data_enviar,
            url: window.link + 'api/exercicio/act-editarprogresso-exercicio.php',
            success: function (retorno) {
                console.log(retorno);
                thisS.parent().children('i').remove();
            }
        });
    });
}
function editarObservacoes(){

    $('#my-tab-content').on('change','.observacaoExercicio', function () {
        var thisS = $(this);
        var id = $(this).parent().parent().attr('id');
        var treino = $(this).parent().parent().parent().parent().parent().parent().attr('id');
        var texto = $(this).val();

        thisS.after('<i class="fa fa-spinner fa-pulse"></i>');

        var data_enviar =
            'uid=' + $.cookie('uid') +
            '&email=' + $.cookie('email') +
            '&treino=' + treino +
            '&ex_id=' + id +
            '&ex_observacao=' + texto;

        $.ajax({
            type: 'post',
            data: data_enviar,
            url: window.link + 'api/exercicio/act-editarobservacao-exercicio.php',
            success: function (retorno) {
                console.log(retorno);
                thisS.parent().children('i').remove();
            }
        });
    });
}
function listarExercicios(){
    $('#exercicioExemplo .categoriaExercicio').html(categoriaExercicio('peso'));

    var data_enviar =
        'uid='      + $.cookie('uid')                 +
        '&email='   + $.cookie('email');

    $.ajax({
        type: 'post',
        data: data_enviar,
        url: window.link+'api/exercicio/act-listar-exercicio.php',
        success: function (retorno){
            console.log(retorno);
            var data = retorno.return;

            if(retorno.reply.code==1) {
                $('#exercicioExemplo').remove();

                $.each(data, function (a, b) {
                    var nomeB = a;

                    if (nomeB > 1) {
                        $('.removerTreino').fadeIn('slow');
                    }

                    if ($('#ref-treino-' + nomeB).length == 0) {
                        $('.addTreino').before('<li class="tabsRefs"><a id="ref-treino-' + nomeB + '" href="#' + nomeB + '" data-toggle="tab">' + window.dias[nomeB - 1] + '</a></li>');
                        $('#my-tab-content').append(
                            '<div class="tab-pane" id="' + nomeB + '" style="border:solid 1px rgb(221, 221, 221);border-top: 0;">' +
                            '    <div class="col-md-12">' +
                            '   <table class="table table-striped table-responsive">' +
                            '   <thead>' +
                            '   <th>Exercício</th>' +
                            '   <th class="Acenter">Categoria</th>' +
                            '   <th class="Acenter">Progresso</th>' +
                            '   <th class="Acenter">Remover</th>' +
                            '   </thead>' +
                            '   <tbody id="exercicios-listar">' +
                            '   <tr id="trNovoExercicio-' + nomeB + '">' +
                            '   <td>' +
                            '   <a href="#" class="addExercicio">Adicionar Exercício</a>' +
                            '</td>' +
                            ' <td class="removerMobile"></td>' +
                            '<td class="removerMobile"></td>' +
                            '<td class="removerMobile"></td>' +
                            '</tr>' +
                            '</tbody>' +
                            '</table>' +
                            '</div>' +
                            '<br clear="all">' +
                            '    </div>'
                        );
                    }

                    $.each(b, function (c, d) {
                        console.log(d);

                        if (!('progresso' in d)) {
                            d.progresso = 'placeholder="1Kg"';
                        } else {
                            d.progresso = 'value="' + d.progresso + '"';
                        }

                        if (!('observacao' in d)) {
                            d.observacao = '';
                        }


                        $('#' + nomeB + ' table tbody').prepend(
                            '<tr id="' + c + '">' +
                            '<td data-title="Exercício">' +
                            '<a href="#" class="editarnome">' +
                            d.nome +
                            '</a>' +
                            '</td>' +
                            '<td data-title="Categoria" class="Acenter categoriaExercicio">' +
                            categoriaExercicio(d.categoria) +
                            '</td>' +
                            '<td data-title="Progresso" class="Acenter">' +
                            '<input ' +
                            'type="text" ' +
                            'style="width:60px;margin-right:5px;" ' +
                            'placeholder="1Kg" class="editarProgressao form-control" ' +
                            d.progresso +
                            '>' +
                            '</td>' +
                            '<td data-title="Observaçōes" class="Acenter">' +
                            '<textarea placeholder=" 4 séries de 12 a 15 repetiçōes" class="observacaoExercicio form-control">' +
                            d.observacao +
                            '</textarea>' +
                            '</td>' +
                            '<td class="text-center">' +
                            '<a ' +
                            'href="#" ' +
                            'data-exercicio="' + c + '" ' +
                            'class="apagarExercicio" ' +
                            'style="color: darkred"' +
                            '>' +
                            '<i class="fa fa-times"></i>' +
                            '</a>' +
                            '</td>' +
                            '</tr>'
                        );
                    })
                });
            }
        }
    });
}
function addNovoExercicio(){
    $(document).on('click','.addExercicio', function () {

        $(this).after(' <i class="fa fa-spinner fa-pulse"></i>');

        var thisS = $(this);
        var id = uniqid();
        var treino = $(this).parent().parent().parent().parent().parent().parent().attr('id');

        var data_enviar =
            'uid='      + $.cookie('uid')   +
            '&email='   + $.cookie('email') +
            '&treino='+treino+
            '&ex_id='+id+
            '&ex_nome=Novo Exercício'+
            '&ex_cat=Peso' +
            '&ex_progresso=';

        $.ajax({
            type: 'post',
            data: data_enviar,
            url: window.link+'api/exercicio/act-ca-exercicio.php',
            success: function (retorno){
                console.log(retorno);
                if(retorno.reply.code==1){
                    var c = retorno.return;
                    thisS.parent().parent().before(
                        '<tr id="'+ id +'">' +
                            '<td data-title="Exercício">' +
                                '<a href="#" class="editarnome">' +
                                    'Novo Exercício'+
                                '</a>' +
                            '</td>' +
                            '<td data-title="Categoria" class="Acenter categoriaExercicio">'+
                                categoriaExercicio()+
                            '</td>' +
                            '<td data-title="Progresso" class="Acenter">' +
                                '<input ' +
                                  'type="text" ' +
                                  'style="width:60px;margin-right:5px;" ' +
                                  'class="editarProgressao form-control" ' +
                                  'placeholder="1Kg"'+
                                '>' +
                            '</td>' +
                            '<td data-title="Observaçōes" class="Acenter">' +
                                '<textarea placeholder=" 4 séries de 12 a 15 repetiçōes" class="observacaoExercicio form-control">'+
                                '</textarea>' +
                            '</td>' +
                            '<td data-title="Remover" class="text-center">' +
                                '<a ' +
                                  'href="#" ' +
                                  'data-exercicio="'+ c.id +'" ' +
                                  'class="apagarExercicio" ' +
                                  'style="color: darkred"' +
                                '>' +
                                    '<i class="fa fa-times"></i>' +
                                '</a>' +
                            '</td>' +
                        '</tr>'
                    );

                    thisS.parent().children('i').remove();
                }
                if(retorno.reply.code == 6){
                    alert('Você precisa assinar um de nossos planos para isso.');
                    thisS.parent().children('i').remove();
                }
            }
        });
    });
}
function categoriaExercicio(x){
    var categorias = ['Peso','Velocidade','Resistencia'];
    var options = '<select class="categoria form-control">';
    var selected = '';
    console.log(x);

    $.each(categorias, function (a,b) {
        if(b==x){
            selected = 'selected';
        }else{
            selected = '';
        }
        options = options + '<option '+selected+' value="'+b+'">'+b+'</option>';
    });

    options = options + '</select>';

    return options;
}
function apagarExercicios(){
    $('#my-tab-content').on('click','.apagarExercicio', function () {
        var thisS = $(this);
        if(confirm('Você tem certeza?')){
            $(this).html('<i class="fa fa-spinner fa-pulse"></i>');
            var exercicio = $(this).data('exercicio');
            var treino = $(this).parent().parent().parent().parent().parent().parent().attr('id');
            var tr = $(this).parent().parent();
            console.log(treino);
            var data_enviar =
                'uid='      + $.cookie('uid')   +
                '&email='   + $.cookie('email') +
                '&treino='+ treino +
                '&ex_id='+exercicio;

            $.ajax({
                type: 'post',
                data: data_enviar,
                url: window.link + 'api/exercicio/act-apagar-exercicio.php',
                success: function (retorno) {
                    if(retorno.reply.code == 1) {
                        tr.fadeOut('slow');
                    }
                    if(retorno.reply.code == 6){
                        alert('Você precisa assinar um de nossos planos para isso.')
                    }
                    thisS.html('<i class="fa fa-times"></i>');
                }
            });
        }
    })
}

function trocarOrganizacaoTreino(){
    $('input[name="radios"]').change(function () {
        $('#tabs li').each(function (a,b) {
            console.log(b);
            var thisS = $(this);
            if($(this).children('a').html() != '+' && $(this).children('a').html() != '-'){
                var nomeB = $(this).children('a').attr('id').substring(11);
                if($('input[name="radios"]:checked').val() == 1){
                    if(nomeB > 7){
                        thisS.children('a').html(window.dias[((nomeB-1)-7)]);
                    }else{
                        thisS.children('a').html(window.dias[nomeB-1]);
                    }
                }else{
                    if(nomeB > 26){
                        thisS.children('a').html(window.letras[((nomeB-1)-26)]);
                    }else{
                        thisS.children('a').html(window.letras[nomeB-1]);
                    }
                }
            }
        })
    });
}

function adicionarTreino(){
    $('.addTreino').click(function () {
        var nomeB = ($('#tabs li').length-1);
        console.log($('input[name="radios"]').val());

        if($('input[name="radios"]:checked').val() == 1){
            if(nomeB > 7){
                var dia = window.dias[((nomeB-1)-7)];
            }else{
                var dia = window.dias[nomeB-1];
            }
        }else{
            if(nomeB > 26){
                var dia = window.letras[((nomeB-1)-26)];
            }else{
                var dia = window.letras[nomeB-1];
            }
        }
        $(this).before('<li class="tabsRefs"><a id="ref-treino-'+nomeB+'" href="#'+nomeB+'" data-toggle="tab">'+ dia +'</a></li>');
        $('#my-tab-content').append(
            '<div class="tab-pane" id="'+nomeB+'" style="border:solid 1px rgb(221, 221, 221);border-top: 0;">'+
            '    <div id="no-more-tables" class="col-md-12">'+
            '   <table class="table table-striped table-responsive">'+
            '   <thead>'+
            '   <th>Exercício</th>'+
            '   <th class="Acenter">Categoria</th>'+
            '   <th class="Acenter">Progresso</th>'+
            '   <th class="Acenter">Remover</th>'+
            '   </thead>'+
            '   <tbody id="exercicios-listar">'+
            '   <tr id="trNovoExercicio-'+nomeB+'">'+
            '   <td>'+
            '   <a href="#" class="addExercicio">Adicionar Exercício</a>'+
            '</td>'+
            ' <td class="removerMobile"></td>'+
            '<td class="removerMobile"></td>'+
            '<td class="removerMobile"></td>'+
            '</tr>'+
            '</tbody>'+
            '</table>'+
            '</div>'+
            '<br clear="all">'+
            '    </div>'
        );
        if(nomeB > 1){ $('.removerTreino').fadeIn("slow"); }
    });
}
function RemoverTreino(){
    $('.removerTreino').click(function () {
        $(this).children('a').append(' <i class="fa fa-spinner fa-pulse"></i>');
        var thisS = $(this);
        var nomeB = $('.tabsRefs').length;
        var treino = $('.tabsRefs').last().children('a').attr('id');
        treino = treino.substr(11,1);
        if(nomeB > 1){
            var data_enviar =
                'uid='      + $.cookie('uid')   +
                '&email='   + $.cookie('email') +
                '&treino='+ treino;

            $.ajax({
                type: 'post',
                data: data_enviar,
                url: window.link + 'api/exercicio/act-apagar-treino.php',
                success: function (retorno) {
                    $('.tabsRefs').last().remove();
                    $('#'+nomeB).remove();
                    thisS.children('a').find('i').remove();
                }
            });
        }
        if(nomeB == 2){
            $('.removerTreino').fadeOut('slow');
        }
    });
}

function uniqid() {
    var ts=String(new Date().getTime()), i = 0, out = '';
    for(i=0;i<ts.length;i+=2) {
        out+=Number(ts.substr(i, 2)).toString(36);
    }
    return ('d'+out);
}