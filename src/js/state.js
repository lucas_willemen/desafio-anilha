
app.config(function($stateProvider, $urlRouterProvider, $mdThemingProvider, $mdDateLocaleProvider){
  $mdDateLocaleProvider.months = ['janvier', 'février', 'mars'];  
  $mdDateLocaleProvider.shortMonths = ['jan', 'fev', 'mar','abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'];
  $mdThemingProvider.theme('default').dark()

  $stateProvider
    .state('inicio', {
      url: "/inicio",
      views: {
        "view": { templateUrl: "inicio.html", controller: function($scope){ $.ajax("framework/js/inicio/js-inicio-global.js?11")   }}
      }
    })  
    .state('perfil', {
      url: "/perfil",
      views: {
        "view": { templateUrl: "perfil.html", controller: function($scope){ $.ajax("framework/js/user/js-user-global.js")   }}
      }
    })  
    .state('diario', {
      url: "/diario",
      views: {
        "view": { templateUrl: "diario.html", controller: function($scope){ $.ajax("framework/js/diario/js-diario-global.js?1")   }}
      }
    }) 
    .state('dieta', {
      url: "/dieta",
      views: {
        "view": { templateUrl: "dieta.html", controller: function($scope){ $.ajax("framework/js/diet/js-diet-global.js")   }}
      }
    }) 
    .state('treino', {
      url: "/treino",
      views: {
        "view": { templateUrl: "treino.html", controller: function($scope){ $.ajax("framework/js/treino/js-treino-global.js")   }}
      }
    })  
    $urlRouterProvider.otherwise("inicio")
  
    
    })

   