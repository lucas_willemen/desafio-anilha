$(function () {
    $("input").on('keypress change', function(){
        var thisE  = $(this);
        var type   = $(this).attr('type');
        var lenght = $(this).val().length;


        switch(type) {
            case 'email':
                if(lenght > 0 && thisE.val().indexOf('@') != -1 && thisE.val().indexOf('.') != -1){
                    valid(thisE);
                } else {
                    invalid(thisE);
                }
                break;
            default:
                if(lenght > 0){
                    valid(thisE);
                } else {
                    invalid(thisE);
                }
        }

        if($('.valid').length >= 3 && $('input[name="termos"]:checked').length > 0){
            $('.send').prop( "disabled", false );
        }else{
            $('.send').prop( "disabled", true );
        }
    });
    $('.send').on('click', function () {
        $('.aj-lo').fadeIn('slow');
        $.ajax({
            type: 'post',
            data: $('.reg-form').serialize(),
            url: window.link + 'api/login&register/act-reg.php',
            success: function (retorno) {
                if(retorno.reply.code == 1){
                    retorno = '<center>Você foi registrado com sucesso!<br>Por favor, cheque seu e-mail.<br></center>';
                }else if(retorno.reply.code == 3){
                    retorno = '<center>Essa conta de usuario já existe, esqueceu sua senha?<br></center>';
                }else{
                    retorno = '<center>Erro ao fazer registro. Por favor, tente novamente mais tarde.<br></center>';
                }
                $('.cnt-cards-form').append(retorno);
                $('.aj-lo').fadeOut('slow');
            }
        });
    });
});