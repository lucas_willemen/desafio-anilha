/* FACEBOOK API */
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '862922937160634',
            xfbml      : true,
            oauth   : true,
            status  : true,
            cookie  : true,
            version    : 'v2.5'
        });
    };
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function fb_login(){
        FB.login(function(response) {
            if (response.authResponse) {
                access_token = response.authResponse.accessToken; //get access token
                user_id = response.authResponse.userID; //get FB UID
                FB.api('me',{fields: 'name,email'}, function(response) {
                    var access_token = FB.getAuthResponse()['accessToken'];
                    $(this).html('Bem vindo ' + response.name);
                    setTimeout(function(){
                        $('#login-modal').modal('hide');
                    }, 1000);
                });

                $.ajax({
                    type: 'post',
                    data: 'token=' + access_token,
                    url: window.link+'api/login&register/act-login-facebook.php',
                    success: function(retorno){
                        var data = retorno;

                        if(data.reply.code != 1){
                            $(this).html(data.reply.comment);
                        }

                        if(data.reply.code == 1){
                            bootbox.alert({
                                message: '<p style="text-align: center;">Bem vindo ' + data.return.nome + '.<p style="font-size: smaller">Redirecionando você...</p></p>',
                                title: '<strong>Login com facebook<strong>'
                            });

                            $.cookie( "email" , data.return.email  , { expires: 7, path: '/' });
                            $.cookie( "nome"  , data.return.nome   , { expires: 7, path: '/' });
                            $.cookie( "uid"   , data.return.uid    , { expires: 7, path: '/' });
                            window.location.replace("inicio.html");
                        }
                    }
                });
            } else {
                $(this).html('Erro no login. Tentar novamente.');
            }
        }, {
            scope: 'public_profile,email,user_website'
        });
    }

    $('#facebookLogin').on('click', function(){
        /*console.log($(this).html());*/
        $(this).html('<i class="fa fa-cog fa-spin fa-fw"></i> Carregando...');
        fb_login();
    });

    $('#face-register').on('click', function(){
        /*console.log($(this).html());*/
        $(this).html('<i class="fa fa-cog fa-spin fa-fw"></i> Carregando...');
        fb_login();
    });

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-68390876-1', 'auto');
    ga('send', 'pageview');

    (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5&appId=862922937160634";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));