$( document ).ready(function() {
   /* pag-croll down button */ 
    $('a.page-scroll').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top - 40
            }, 900);
            return false;
          }
        }
      });

    /* on window scroll fix navbar */
    $(window).bind('scroll', function() {
        var navHeight = $(window).height() - 350;
        if ($(window).scrollTop() > navHeight) {
            $('.navbar-default').addClass('on');
        } else {
            $('.navbar-default').removeClass('on');
        }
    });

    $('body').scrollspy({ 
        target: '.navbar-default',
        offset: 80
    });

    /* modal for services */
    $('a[rel="modal-toggle"]').click(function(event) {
        var service = $(this).data('service');
        var title = '';
        var content = '';
        switch(service) {
            case 'diary':
                var title = 'Diário';
                var content = '<div style="margin-top: 20px;" class="col-md-12"><p>O do Anilha sempre acompanha você. Não perca nenhuma caloria, de um jeito muito fácil você tem um relatório completo de toda sua ingestão de macronutrientes. Descubra sobre a velocidade do seu metabolismo e determine metas, acerte na sua dieta.</p>'+
                    '<h3>Para o usuário iniciante:</h3>'+
                    '<ul>'+
                    '<li>Tenha relatórios precisos sobre sua alimentação diária</li>'+
                    '<li>Estipule metas e faça previsões de ganhos e perdas de peso</li>'+
                    '<li>Ajuste sua dieta para atingir seus objetivos</li>'+
                    '<h3>Para profissionais:</h3>'+
                    '<li>Tenha controle total da alimentação do seu paciente</li>'+
                    '<li>Controle os macronutrientes e ingestão calórica</li>'+
                    '<li>Acompanhe o desenvolvimento do seu metabolismo</li>'+
                    '</ul></div>';
                break;
            case 'diet':
                var title = 'Dieta';
                var content = '<div style="margin-top: 20px;" class="col-md-12"><p>Basta de promessas absurdas de revistas ("emagreça 15kg em 3 dias"), que não funcionam! Aqui no Anilha fazemos tudo para você alcançar seu objetivo. Diferente de dietas fajutas, o anilha gera sua dieta em segundos, graças ao processamento da nuvem sua dieta é calculada via inteligência artificial e uma série de algoritmos. A dieta é exclusiva para você, conforme seu metabolismo e dia-a-dia.</p>'+
                    '<h3>Para o usuário iniciante:</h3>'+
                    '<ul>'+
                    '<li>Gere uma dieta em segundos e que funciona.</li>'+
                    '<li>Monte uma dieta que acompanha seu metabolismo</li>'+
                    '<li>Sua dieta é desenvolvida exclusiva para, conforme seus dados</li>'+
                    '<li>Leve sua dieta sempre com você</li>'+
                    '</ul>'+
                    '<h3>Para profissionais:</h3>'+
                    '<ul>'+
                    '<li>Controle a dieta de seus pacientes </li>'+
                    '<li>Controle os macronutrientes exatos da dieta</li>'+
                    '<li>Acompanhe o desenvolvimento do seu paciente/aluno</li>'+
                    '</ul></div>';
                break;
            case 'workout':
                var title = 'Treino';
                var content = '<div style="margin-top: 20px;" class="col-md-12"><p>Você faz musculação? Corre? Luta? Joga futebol? Como você controla seu treino? No Anilha não importa a modalidade. Você consegue controlar o treino de qualquer modalidade esportiva e estipular metas, registrar progressos e exercícios. Tudo isso sempre com você, acesso a qualquer minuto.</p>'+
                    '<h3>Para o usuário iniciante:</h3>'+
                    '<ul>'+
                    '<li>Jamais esqueça um exercício</li>'+
                    '<li>Estipule metas e conquiste-as!</li>'+
                    '</ul>'+
                    '<h3>Para profissionais:</h3>'+
                    '<ul>'+
                    '<li>Tenha controle total do treino do seu aluno</li>'+
                    '<li>Faça alterações no treino do seu aluno e ele recebe na hora</li>'+
                    '<li>Tenha o relatório detalhado do progresso do aluno</li>'+
                    '</ul></div>';
                break;
            case 'media':
                var title = 'Mídias sociais e de informação';
                var content = '<div style="margin-top: 20px;" class="col-md-12"><p>Disponibilizamos conteúdos sobre o mundo fitness gratuitamente, via redes sociais (Facebook, Twitter, Youtube) e o blog do Anilha. Estude e compreenda cada vez mais sobre o corpo humano, nutrição e educação física.</p>'+
                    '<h3>Para o usuário iniciante:</h3>'+
                    '<ul>'+
                    '<li>Obtenha conhecimento de qualidade gratuitamente</li>'+
                    '<li>Evolua cada vez mais e passe a entender seu corpo.</li>'+
                    '</ul>'+
                    '<h3>Seja um contribuidor:</h3>'+
                    '<ul>'+
                    '<li>Dê um UP no currículo</li>'+
                    '<li>Ganhe visibilidade no mercado</li>'+
                    '<li>Compartilhe seu conhecimento com milhares de pessoas</li>'+
                    '</ul></div>';
                break;
        }
        bootbox.alert({
            message: content,
            title: '<strong>' + title + '<strong>'
        });
        return false;
    });
});