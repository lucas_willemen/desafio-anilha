var app = angular.module('App',['ngMaterial', 'ngMessages', 'ui.router'])

app.controller('Main', function($scope, $mdDialog, $http, $mdToast) {
  $mdToast.show(
    $mdToast.simple()
    .content("Bem vindo ao Anilha. Nosso desafio, é o seu!")
    .position('top right')
    .hideDelay(3000)
    );
  $scope.login = function(ev) {
    $mdDialog.show({
      controller: logincontroller,
      templateUrl: 'modal/login.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
        .then(function(answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function() {
          $scope.status = 'You cancelled the dialog.';
        });
  };
 function logincontroller($scope, $mdDialog) {
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
}
function buscarAlimentos(){
    $(document).on('keyup change','input[name="buscar-alm"],#buscar-alm', function () {
        var value = $(this).val();
        var thisS = $(this);
        if($(this).val().length % 3 == 0){
            $('input[name="buscar-alm"],#buscar-alm').prop('disabled',true);
            $('input[name="btn-buscar-alm"],#btn-buscar-alm').html('<i class="fa fa-cog fa-spin"></i>');

            $.getJSON( window.link + "api/diet/act-get-alimento.php?nome=" + value, function( data ) {
                var items = [];
                $.each( data.return, function( key, val ) {
                    items.push({label: val.nome, value: val.uid});
                });

                $('input[name="buscar-alm"],#buscar-alm').prop('disabled',false);
                $('input[name="btn-buscar-alm"],#btn-buscar-alm').html('<i class="fa fa-search"></i>');

                thisS.autocomplete({
                    select: function (a, b) {
                        window.alm_uid = b.item.value;
                        window.alm_qnt = 100;

                        /*
                        * função inclui a página alimen.html e após chama o modal.   
                        */
                       
                        $(function () {
                            $.get('template/alimentos.html', function (data) {
                                bootbox.alert({
                                    message: data,
                                    title: '<h4 id="alm-nome"></h4>'
                                });
                            });
                        });
                    },
                    source: items,
                    close: function( event, ui ) {
                        $('input[name="buscar-alm"],#buscar-alm').val('');
                    }
                });
            });

        }
    });
} buscarAlimentos();
});
