/*--------------------------------------------
|                                             |
|    A N I L H A - Front END Framework V 0.1  |
|                                             |
 --------------------------------------------*/

    if(!($.cookie("uid"))){ window.location.replace("index.html"); }

    window.link = 'http://api.anilha.net/';
    window.userjson = "http://anilha.net/users/" + $.cookie('uid') + ".json";
    window.json = '';

    $(function () {
       
        
        load_pages();
        logout();
    });

    $(window).load(function () { $('.load').fadeOut('slow'); });

    function load_pages(){
        $('#footer').load('template/footer.html');
    }
    function write(json,to){
        if(json) { to.val(json); }
    }
    function header(){
        if ('nome' in window.json) {
            if($('title').html().indexOf(window.json.nome) < 0){$('title').append(window.json.nome);}
            $('.account,#Account,.navbar-nav .account').html('<i class="fa fa-user"></i> '+ window.json.nome + '<b class="caret"></b>');
        }

        if ('avisos' in window.json) {
            $('.navbar-nav #avisos_conta,#avisos_conta').html($(window.json.avisos).size());

            switch ($(window.json.avisos).size()){
                case 0:
                    $('.navbar-nav #avisos_conta,#avisos_conta').parent().fadeOut('slow');
                break;
                default:
                    $.each(window.json.avisos, function (key,value) {
                        var avisos = value.split(';;');
                        $('.navbar-nav #avisos,#avisos').append('<li><a href="' + avisos[0] + '">' + avisos[1] + '</a></li>');
                    });
                break;
            }

        }else{
            $('.navbar-nav #avisos_conta,#avisos_conta').parent().fadeOut('slow');
        }

        if('dias' in window.json){
            if(window.json.dias == 0){
                $('#menu-diario a,#menu-dieta a,#menu-treino a').css('color','#ccc');
                $('.copyright').html('Você tem '+ window.json.dias + ' dias. <a href="#">Renovar</a>')
            }
            $('.dias').html(window.json.dias);
        }else{
            $('.dias').html('0');
            $('#menu-diario a,#menu-dieta a,#menu-treino a').css('color','#ccc');
        }

    }
    function email_confirm(){
        if ('email_verificado' in window.json && window.json['email_verificado'] == 0) {
            $.get("/template/email-confirm.html", function(data) {
                $('.content > .container-fluid').prepend(data);
                $('#E-email').html(window.json.email);
            });
        }
    }
    function JsonRequired(){
        header();

        email_confirm();
        $('.impressao').attr(
            'href',
            'http://api.anilha.net/api/diet/act-dieta-file.php?' +
            '9871d3a2c554b27151cacf1422eec048='+window.json.uid+'&' +
            '0c83f57c786a0b4a39efab23731c7ebc='+window.json.email +
            '&tipo=pdf'
        );
        $('.csv').attr(
            'href',
            'http://api.anilha.net/api/diet/act-dieta-file.php?' +
            '9871d3a2c554b27151cacf1422eec048='+window.json.uid+'&' +
            '0c83f57c786a0b4a39efab23731c7ebc='+window.json.email +
            '&tipo=csv'
        );
        $('#jsonload').fadeOut('slow');
        console.log(window.json);
    }
    function logout(){
        $('#logout').click(function () {
            $.removeCookie("nome", { path: '/' });
            $.removeCookie("email", { path: '/' });
            $.removeCookie("uid", { path: '/' });
            window.location.replace("index.html");
        });
    }
    

    window.ucwords = function(str) {
        return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
            return $1.toUpperCase();
        });
    };
    window.preencher_form = function(){
        write(window.json.nome, $('#nome'));
        write(window.json.email, $('#email'));
        if ('email' in window.json) { $('#E-email').html(window.json.email); }
        if ('perfil' in window.json){
            if ('cep'       in window.json.perfil){
                write(window.json.perfil.cep, $('#cep'));
            }
            if ('pais'      in window.json.perfil){
                write(window.json.perfil.pais, $('#pais'));
            }
            if ('altura'    in window.json.perfil){
                write(window.json.perfil.altura, $('#altura'));
            }
            if ('cidade'    in window.json.perfil){
                write(window.json.perfil.cidade, $('#cidade'));
            }
            if ('endereco'  in window.json.perfil){
                write(window.json.perfil.endereco, $('#endereco'));
            }
            if ('refeicoes' in window.json.perfil){
                write(window.json.perfil.refeicoes, $('#refeicoes'));
            }

            if ('FA' in window.json.perfil){
                $('select#FA option').removeAttr('selected')
                    .filter('[value="'+window.json.perfil['FA']+'"]')
                    .prop('selected', true);
            }
            if ('sexo'  in window.json.perfil){
                var sexo = window.json.perfil.sexo;
                if (sexo == 'M') {
                    $('#M').prop('checked', true);
                }
                if (sexo == 'F') {
                    $('#F').prop('checked', true);
                }
            }
            if ('idade' in window.json.perfil){
                var d = new Date();
                $('#idade').val(d.getFullYear() - window.json.perfil.idade);
            }
            if ('peso'  in window.json.perfil){
                $.each(window.json.perfil.peso.slice(-1).pop(), function (k, v) {
                    $('#peso').val(k);
                });
            }
            if ('alimentos_preferidos' in window.json.perfil){
                $.each(window.json.perfil.alimentos_preferidos, function (k, v) {
                    console.log(v);
                    $('#alimentos-preferidos').append('<option value="' + v + '">' + v + '</option>');
                });
            }
            if ('alimentos_proibidos'  in window.json.perfil){
                $.each(window.json.perfil.alimentos_proibidos, function (k, v) {
                    $('#alimentos-proibidos').append('<option value="' + v + '">' + v + '</option>');
                });
            }
        }
    };

$(document).ready(function() {
    $.ajaxSetup({ cache: false });
});