/*--------------------------------------------
 |                                             |
 |    A N I L H A - Front END Framework V 0.1  |
 |                                             |
 --------------------------------------------*/

    function enviar_ajax() {
        $('#atualizar-perfil').click(function () {
            $('#atualizar-perfil-working').fadeIn('fast');
            $.ajax({
                type: 'post',
                data: $('#perfil-form').serialize(),
                url: window.link + 'api/user/act-perfil.php?uid=' + window.json.uid + '&email=' + window.json.email,
                success: function (retorno) {
                    console.log(retorno);
                    $('#atualizar-perfil-working').fadeOut('slow');
                    $('#atualizar-perfil-resultado').append('<font style="font-size: smaller">Perfil Atualizado.</font>');
                }
            });
        });
        
    }
    function alimentos_add() {
        var categorias = [];
        $.getJSON( "/framework/json/categorias.json", function( data ) {
            categorias = data["content"];
            console.log(categorias);
            $(".autocomplete").autocomplete({
                source: categorias
            });
        });

        $('.alm-add').on("autocompleteselect", function (event, ui) {
            $(this).parent().parent().find("select").append('<option value="' + ui.item.value + '">' + ui.item.value + '</option>');
        }).on("autocompleteclose", function (event, ui) {
            $(this).val('')
        });

        $('select').keypress(function (e) {
            if (e.which == 8 || e.which == 46) {
                $(this).find('option:selected').remove();
            }
        });

        $('#atualizar-alimentos-preferidos').click(function () {
            var alimentos_preferidos = '';
            $('#alimentos-preferidos option').each(function () {
                if ($(this).val().length > 0) {
                    alimentos_preferidos += $(this).val() + ';';
                }
            });
            $(this).children('i').fadeIn('fast');
            $.ajax({
                type: 'post',
                data: 'preferidos=' + alimentos_preferidos,
                url: window.link + 'api/user/act-alimentos.php?uid=' + window.json.uid + '&email=' + window.json.email + '&alm=preferidos',
                success: function (retorno) {
                    JSON.parse(JSON.stringify(retorno));
                    console.log(retorno.reply);
                    if (retorno.reply.code == 1) {
                        $('#atualizar-alimentos-preferidos').parent().append('<p style="font-size: smaller">Atualizado com sucesso.</p>');
                    } else {
                        $('#atualizar-alimentos-preferidos').parent().append('<p style="font-size: smaller">Erro ao atualizar.</p>');
                    }
                    $('#atualizar-alimentos-preferidos').children('i').fadeOut('slow');
                }
            });
            console.log(alimentos_preferidos);
        });
        $('#atualizar-alimentos-proibidos').click(function () {
            var alimentos_proibidos = '';
            $('#alimentos-proibidos option').each(function () {
                if ($(this).val().length > 0) {
                    alimentos_proibidos += $(this).val() + ';';
                }
            });
            $(this).children('i').fadeIn('fast');
            $.ajax({
                type: 'post',
                data: 'proibidos=' + alimentos_proibidos,
                url: window.link + 'api/user/act-alimentos.php?uid=' + window.json.uid + '&email=' + window.json.email + '&alm=proibidos',
                success: function (retorno) {
                    JSON.parse(JSON.stringify(retorno));
                    console.log(retorno.reply);
                    if (retorno.reply.code == 1) {
                        $('#atualizar-alimentos-proibidos').parent().append('<p style="font-size: smaller">Atualizado com sucesso.</p>');
                    } else {
                        $('#atualizar-alimentos-proibidos').parent().append('<p style="font-size: smaller">Erro ao atualizar.</p>');
                    }
                    $('#atualizar-alimentos-proibidos').children('i').fadeOut('slow');
                }
            });
            console.log(alimentos_proibidos);
        });
    }

    $.getJSON( window.userjson, function( data ) {
        window.json = data;
        console.log(data);
        JsonRequired();
        $('#enviar-email').click(function () {
            $.ajax({
                type: 'post',
                data: '',
                url: window.link + 'api/user/act-confirm-email.php?uid=' + window.json.uid + '&email=' + window.json.email,
                success: function (retorno) {
                    console.log(retorno);
                    alert('Email enviado com sucesso.');
                }
            });
        });

        enviar_ajax();
        window.preencher_form();
    })
    .fail(function() {
        $.removeCookie("nome");
        $.removeCookie("email");
        $.removeCookie("uid");
        window.location.replace("index.html");
    });

    $(window).load(function() {
        alimentos_add();
        gerarLinks();

        $('input[type="number"]').keypress(function(event) {
            var tecla = (window.event) ? event.keyCode : event.which;
            if ((tecla > 47 && tecla < 58)) return true;
            else {
                if (tecla != 8) return false;
                else return true;
            }
        });
    });



function gerarLinks(){
    $.ajax({
        type: 'post',
        data: 'uid='+ $.cookie('uid') + '&email=' + $.cookie('email'),
        url: window.link + 'api/pagamento/pagamento-gerarlinks.php',
        success: function (retorno) {
            console.log(retorno);
            $('#pay30').attr('href',retorno.return['30']).attr('disabled',false);
            $('#pay90').attr('href',retorno.return['90']).attr('disabled',false);
            $('#pay365').attr('href',retorno.return['365']).attr('disabled',false);
        }
    });
}