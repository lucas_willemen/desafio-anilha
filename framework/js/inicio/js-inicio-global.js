$(document).ready(function(){

    if(!sessionStorage.getItem("notify")){
        $.notify({
            icon: 'fa fa-hand-peace-o',
            message: "Bem vindo ao <b>Anilha</b>. Nosso desafio, é o seu!"

        },{
            type: 'info',
            timer: 800
        });
        sessionStorage.setItem("notify","close");
    }

    $.getJSON(window.userjson, function( json ) {
        window.json = json;

        JsonRequired();
        if('perfil' in json){
            $('#atualiar-peso-div').fadeIn('slow');
            $.each(json.perfil.peso.slice(-1).pop(), function (k,v) {
                $('#peso').attr('placeholder',k);
            });
        }else{
            $('#atualizar-perfil').fadeIn('slow');
        }

        var label_graf = [];
        var data_graf = [];

        if('perfil' in json && 'peso' in json.perfil && json.perfil.peso.length >= 2){
            $('#evolucao').fadeIn('slow');

            if(json.perfil.peso.length > 10){
                var j = json.perfil.peso.length - 10;
            }else{
                var j = 0;
            }

            for (i = j; i < json.perfil.peso.length; i++) {
                $.each(json.perfil.peso[i], function (a, b) {
                    b = b.split("/");
                    label_graf.push(b[0] + '/' + b[1]);
                    data_graf.push(parseInt(a));
                });
            }

            console.log(label_graf);
            console.log(data_graf);

            var chart = new Chartist.Line('#grafico-peso', {
                labels: label_graf,
                series: [{
                    name: 'series-2',
                    data: data_graf
                }]
            }, {
                fullWidth: true,
                series: {
                    'series-2': {
                        lineSmooth: Chartist.Interpolation.simple(),
                        showArea: true
                    }
                },
                axisX: {
                    showGrid: false
                },
                showLine: false,
                showPoint: false
            }
            [
                ['screen and (max-width: 320px)', {
                    series: {
                        'series-2': {
                            lineSmooth: Chartist.Interpolation.none(),
                            showArea: false
                        }
                    }
                }]
            ]);
        }
        if('dieta' in json){
            $('#ajustar-dieta').fadeIn('slow');
            var dieta = window.json.dieta[(window.json.dieta.length)-1];
            var i = 0 ;
            $.each(dieta.refeicao, function (a,b) {
                $('#dieta-rapida').fadeIn('slow');
                if(i==0){var active = 'active'}else{var active = '';}
                $('#tabs').append(
                    ' <li class="'+ active + '">'                   +
                        '<a href="#ref-'+ a +'" data-toggle="tab">' +
                            a                                       +
                        '</a>'                                      +
                    '</li>'
                );

                $('#my-tab-content').append(
                    '<div '                                                                         +
                        'class="tab-pane '+ active + '" '                                           +
                        'id="ref-'+ a +'" '                                                         +
                        'style="border:solid 1px rgb(221, 221, 221);border-top: 0;"'   +
                    '>'                                                                             +
                        '<table class="table table-responsive table-striped">'                      +
                            '<thead>'                                                               +
                                '<tr>'                                                              +
                                '<th>Alimento</th>'                                                 +
                                '<th style="text-align: center">Quantidade</th>'                    +
                                '</tr>'                                                             +
                            '</thead>'                                                              +
                            '<tbody id="tbody-ref-'+ a +'">'                                        +
                            '</tbody>'                                                              +
                        '</table>'                                                                  +
                    '</div>'
                );

                $.each(b.alimentos, function (s,g) {
                    $('#tbody-ref-'+ a).append(
                        '<tr>'                                  +
                            '<td>' + g.nome + '</td>'           +
                            '<td style="text-align: center">'   +
                                g.quantidade + ' g '            +
                            '</td>'                             +
                        '</tr>'
                    );
                });

                i++;
            });
        }
    })
        .done(function() {
            console.log( "second success" );
        })
        .fail(function() {
            //window.location.replace("index.html");
        });
});

$(function () {
    $('#peso').on('change keyup',function () {
        if ( $.trim($('#peso').val()).length > 0 ){
            $('#atualizar-peso').prop('disabled',false);
        }else{
            $('#atualizar-peso').prop('disabled',true);
        }
    });

    $('#atualizar-peso').on('click', function () {
        $('#atualizar-peso').html('Carregando...');

        if($('#ajustar-dieta').is(':checked')){
            var data_ajax = 'uid=' + $.cookie('uid') + '&email=' + $.cookie('email') + '&peso=' + $('#peso').val() + '&ajustar=1';
        }else{
            var data_ajax = 'uid=' + $.cookie('uid') + '&email=' + $.cookie('email') + '&peso=' + $('#peso').val();
        }

        $.ajax({
            type: 'post',
            data: data_ajax,
            url: window.link + 'api/user/act-peso.php',
            success: function (retorno) {
                $('#atualizar-peso').html('Atualizar');
                if(retorno.reply.code == 1){
                    $('#peso').attr(
                        'placeholder',
                        $('#peso').val()
                    ).val('');
                    $('#footer-atualizar').append('<p style="font-size: smaller">Peso atualizado com sucesso.</p>');
                }
            }
        });

    });
});