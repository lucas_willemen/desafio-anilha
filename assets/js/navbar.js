var url = window.location.href;

window.sidebar = function(){
	window.checkSize();

	$(window).resize(window.checkSize);

	$('#menu').on('click', function(evt){
		console.log(evt.data('link'));
		return false;
	});

	$(window).load(function () { $('.load').fadeOut('slow'); });
	/*console.log(window.spliteUrl()[0]);*/

	window.activeItem();

	$('#sidebar li').hover(function(){
	    	if ($(window).width() > 768)
	    		$(this).css('border-left', '10px solid'+ window.generateColor());
	    }, function(){
	    $(this).css('border-left', '');
	});

};

function checkSize(){
    if ($(window).width() <= 768){	
		$('#sidebar').css({
			'background-image': ''
		});

		$('nav').removeClass('navbar-fixed-top').css({
			'margin-top': '-100px',
			'margin-bottom': '-1px'
		});
	} else {
		$('#sidebar').css({
			'background-image': 'url('+ $('#sidebar').data('image') +')'
		});

		$('nav').addClass('navbar-fixed-top').css({
			'margin-top': '',
			'margin-bottom': ''
		});;
	}
}

function spliteUrl(){
	var url = window.url.split('/');
	return url;
}

function activeItem(){
	var name = window.spliteUrl()[3];
	name = name.split('.');

	$('#menu-'+ name[0]).addClass('active');
	window.activeBreadcrumb(name[0]);
}

function activeBreadcrumb(name){
	$('#breadcrumb').html(name);
}

function generateColor(){
	var colorArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
	var color = new Array();

	for (var i = 0; i < 6; i++) {
		color.push(colorArray[Math.round(Math.random() * (colorArray.length - 1))]);
	};

	return '#'+color[0]+color[1]+color[2]+color[3]+color[4]+color[5];
}