window.link = 'http://api.anilha.net/';

$( document ).ready(function() {


    function preloadImages(arrayOfImages) {
        $(arrayOfImages).each(function () {
            $('<img class="preload" />').attr('src',this).appendTo('body').css('display','none');
        });
        $('.preload').one("load", function() {
            if (this.complete) {
                console.log('.');
            }
        });
    }

    var imageSrcs = [
        "assets/img/index/1920px/background.jpg",
        "assets/img/index/1920px/background2.jpg",
        "assets/img/index/1920px/background3.jpg"
    ];

    $.when(preloadImages(imageSrcs)).then(function () {

        var i = 2;

        setInterval(function(){
            var array = {
                1: {
                    desc: 'mudar sua vida',
                    bg: 'assets/img/index/1920px/background.jpg'
                },
                2: {
                    desc: 'mudar seu esporte',
                    bg: 'assets/img/index/1920px/background1.jpg'
                },
                3: {
                    desc: 'mudar seu rendimento',
                    bg: 'assets/img/index/1920px/background2.jpg'
                },
                4: {
                    desc: 'mudar sua alimentação',
                    bg: 'assets/img/index/1920px/background3.jpg'
                }
            };

            var item = array[i];

            console.log(item);

            $('#mudar').fadeOut(500, function () {

                $('#tf-home').css('backgroundImage','url('+item.bg+')');
                $(this).html(item.desc).fadeIn(500);
            });
            i++;

            if(i>4){
                i = 1;
            }

        }, 8000);
    });

    /* user logged-in verification */
    if($.cookie("email") && $.cookie("nome") && $.cookie("uid")){
        window.location.replace("inicio.html");
    }

    /* calling login form */

    $('#login-form').formValidation({
        framework: 'bootstrap',
                icon: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-remove',
                    validating: 'fa fa-refresh fa-spin'
                },
        fields: {
            email: {
                validators: {
                    notEmpty: {
                        message: 'O email não pode ficar vazio!'
                    },
                    emailAddress: {
                        message: 'Este não é um endereço de email válido.'
                    },
                    stringLength: {
                        min: 10,
                        message: 'Email deve ser maior do que 10 digitos!'
                    }
                }
            },
            senha: {
                validators: {
                    notEmpty: {
                        message: 'A senha não pode ficar vazia!'
                    },
                    stringLength: {
                        min: 5,
                        message: 'Sua senha deve ser maior do que 5 digitos!'
                    }
                }
            }
        }
    }).on('prevalidate.form.fv', function(e) {
        $('#callback-form').hide();
        $('#btn-submit').html('<i class="fa fa-cog fa-spin fa-fw"></i> Enviar');
    }).on('err.form.fv', function(e) {
        $('#btn-submit').html('Acessar sistema');
    }).on('success.form.fv', function(e) {
        e.preventDefault();
        var $form = $(e.target);
        $.ajax({
            type: 'post',
            data: $form.serialize(),
            url: 'http://api.anilha.net/api/login&register/act-login.php',
            success: function(retorno){
                $form.formValidation('resetForm', true);
                var data = jQuery.parseJSON(JSON.stringify(retorno));
                console.log(retorno);
                if(data.reply.code == 4){
                    $('#callback-form').fadeIn();
                    $('#callback-form').html('Usuário ou senha incorretos');
                    $('#btn-submit').html('Acessar sistema');
                }
                if(data.reply.code == 1){
                    $('#callback-form').fadeIn();
                    $('#callback-form').html(''                                         +
                        '<center>'                                                      +
                            '<p>Bem vindo '                                             +
                                data.return.nome                                        +
                            '.</p>'                                                     +
                            '<p style="font-size: smaller">Redirecionando você...</p>'  +
                        '</center>'
                    );
                    $('#btn-submit').html('Acessar sistema');
                    $.cookie( "email" , data.return.email , { expires: 7, path: '/' });
                    $.cookie( "nome"  , data.return.nome  , { expires: 7, path: '/' });
                    $.cookie( "uid"   , data.return.uid   , { expires: 7, path: '/' });
                    window.location.replace("inicio.html");
                }
            }
        });
    });


    $('#register-form').formValidation({
        framework: 'bootstrap',
                icon: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-remove',
                    validating: 'fa fa-refresh fa-spin'
                },
        fields: {
            ca_email: {
                validators: {
                    notEmpty: {
                        message: 'O email não pode ficar vazio!'
                    },
                    emailAddress: {
                        message: 'Este não é um endereço de email válido.'
                    },
                    stringLength: {
                        min: 10,
                        message: 'Email deve ser maior do que 10 digitos!'
                    }
                }
            },
            ca_nome: {
                validators: {
                    notEmpty: {
                        message: 'O nome não pode ficar vazio!'
                    },
                    stringLength: {
                        min: 3,
                        message: 'Seu nome deve ser maior do que 3 digitos!'
                    }
                }
            },
            ca_psswd: {
                validators: {
                    notEmpty: {
                        message: 'A senha não pode ficar em branco!'
                    },
                    stringLength: {
                        min: 5,
                        message: 'Sua senha deve ser maior do que 5 digitos!'
                    }
                }
            },
            rep_psswd: {
                validators: {
                    notEmpty: {
                        message: 'A senha não pode ficar em branco!'
                    },
                    identical: {
                        field: 'ca_psswd',
                        message: 'A senha e a repetição devem ser as mesmas!'
                    }
                }
            }
        }
    }).on('prevalidate.form.fv', function(e) {
        $('#btn-register').html('<i class="fa fa-cog fa-spin fa-fw"></i> Cadastrar-se');
    }).on('err.form.fv', function(e) {
        $('#btn-register').html('Cadastrar-se');
    }).on('success.form.fv', function(e) {
        e.preventDefault();
        var $form = $(e.target);
        $.ajax({
            type: 'post',
            data: $form.serialize(),
            url: window.link + 'api/login&register/act-reg.php',
            success: function (retorno) {
                console.log(retorno);
                var data = retorno;
                $form.formValidation('resetForm', true);
                $('#btn-register').html('Cadastrar-se');
                if (data.reply.code == 1) {
                    bootbox.alert({
                        message: '<p style="text-align: center;">Você foi cadastrado com sucesso, por favor cheque seu email.</p>',
                        title: '<strong>Confirmação de cadastro<strong>'
                    });
                }else{
                    bootbox.alert({
                        message: '<p style="text-align: center;">' + retorno.reply.comment + '</p>',
                        title: '<strong>Erro no registro<strong>'
                    });
                }
            }
        });
    });

    /* contact form */

    $('#contact-form').formValidation({
        framework: 'bootstrap',
                icon: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-remove',
                    validating: 'fa fa-refresh fa-spin'
                },
        fields: {
            nome: {
                validators: {
                    notEmpty: {
                        message: 'O nome não pode ficar vazio!'
                    },
                    stringLength: {
                        min: 3,
                        message: 'Seu deve ser maior do que 3 digitos!'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'O email não pode ficar vazio!'
                    },
                    emailAddress: {
                        message: 'Este não é um endereço de email válido.'
                    },
                    stringLength: {
                        min: 10,
                        message: 'Email deve ser maior do que 10 digitos!'
                    }
                }
            },
            titulo: {
                validators: {
                    notEmpty: {
                        message: 'O titulo não pode ficar vazio!'
                    },
                    stringLength: {
                        min: 5,
                        message: 'O titulo deve ser maior do que 5 digitos!'
                    }
                }
            },
            mensagem: {
                validators: {
                    notEmpty: {
                        message: 'A mensagem não pode ficar vazia!'
                    },
                    stringLength: {
                        min: 20,
                        message: 'Sua senha deve ser maior do que 20 digitos!'
                    }
                }
            }
        }
    }).on('success.form.fv', function(e) {
        e.preventDefault();
        var $form = $(e.target);
        $.ajax({
            type: 'post',
            data: $form.serialize(),
            url: window.link + 'api/email/act-email-contato.php',
            success: function (retorno) {
                var data = retorno;
                $form.formValidation('resetForm', true);
                if (data.reply.code == 1) {
                    bootbox.alert({
                        message: '<p style="text-align: center;">Email enviado com sucesso.</p>',
                        title: '<strong>Confirmação de Contato<strong>'
                    });
                }else{
                    bootbox.alert({
                        message: '<p style="text-align: center;">' + retorno.reply.comment + '</p>',
                        title: '<strong>Erro ao enviar email!<strong>'
                    });
                }
            }
        });
    });
});

$(window).load(function () {
    $.when( $( "#load" ).load( "template/index-load.html" ) ).then(function () {
        function preloadImages(arrayOfImages) {
            $(arrayOfImages).each(function () {
                $('<img class="preload" />').attr('src',this).appendTo('body').css('display','none');
            });
            $('.preload').one("load", function() {
                if (this.complete) {
                    console.log('.');
                }
            });
        }

        var j = 0;

        var array = ['assets/img/index/1920px/imac.png','assets/img/index/1920px/imac2.png','assets/img/index/1920px/tablet.png','assets/img/index/1920px/phone.png'];

        $.when(preloadImages(array)).then(function () {
            setInterval(function(){
                $('#tf-plataform-img').animate({opacity: 0}, 1000, function () {
                    $('#tf-plataform-img').attr('src',array[j]).one("load", function() {
                        if(this.complete){
                            $('#tf-plataform-img').animate({opacity: 1}, 1000);
                        }
                    });

                    if(j < 2){
                        $('#fa-plataform-selector .active').removeClass('active');
                        $('#fa-plataform-selector span:first-child').addClass('active');
                    }
                    if(j == 2){
                        $('#fa-plataform-selector .active').removeClass('active');
                        $('#fa-plataform-selector span:nth-child(2)').addClass('active');
                    }
                    if(j == 3){
                        $('#fa-plataform-selector .active').removeClass('active');
                        $('#fa-plataform-selector span:last-child').addClass('active');
                    }
                });

                j++;
                if(j >= array.length){
                    j = 0;
                }
            },6000);
        });


        var services = {
            diet:'Basta de promessas absurdas de revistas ("emagreça 15kg em 3 dias"), que não funcionam! Aqui no Anilha fazemos tudo para você alcançar seu objetivo. O anilha gera sua dieta em segundos; graças ao processamento da nuvem sua dieta é calculada via inteligência artificial e uma série de algoritmos. A dieta é exclusiva para você, conforme seu metabolismo e dia-a-dia.',
            daily:'O diário do Anilha sempre acompanha você! Não esqueça de nenhuma caloria, de um jeito muito fácil você anota sua alimentação e obtém um relatório completo de toda sua ingestão de macronutrientes.Descubra sobre a velocidade do seu metabolismo, estipule metas e acerte na sua dieta!',
            workout:'Você faz musculação? Corre? Luta? Joga futebol? Como você controla seu treino? No Anilha não importa a modalidade. Você consegue controlar o treino de qualquer modalidade esportiva e estipular metas, registrar progressos e exercícios. Tudo isso sempre com você, acessível a qualquer minuto.',
            media:'Disponibilizamos conteúdos sobre o mundo fitness gratuitamente, via redes sociais (Facebook, Twitter, Youtube) e o blog do Anilha. Estude e compreenda cada vez mais sobre o corpo humano, nutrição e educação física.'

        };

        $(document).on('mouseover click','.services-item', function () {
            $('.services-active').removeClass('services-active');
            $(this).addClass('services-active');
            $('#services-description').html(services[$(this).data('text')]);
        });

    });
});