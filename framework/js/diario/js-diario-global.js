$(function () {
    window.totais_diario = {
        carboidratos: 0,
        proteinas: 0,
        gorduras: 0,
        total:0
    };

    $('#copiarRefeicao').on('click', function (e) {
        e.preventDefault();
        copiarDieta();
        return false;
    });

    var d          = new Date();
    var month      = d.getMonth()+1;
    var day        = d.getDate();
    var data       = (day<10 ? '0' : '') + day + '/' + (month<10 ? '0' : '') + month + '/' + d.getFullYear();

    $('#diario-datas')
        .datepicker({ dateFormat: "dd/mm/yy" })
        .val(data);

    addRefeicao();
    removerRefeicao();
    removerAlimento();
    diarioSelectChange();

    $.carregarDiario();
    $.getjson();

    $('a[rel="faceboxnew"]').click(function (event) {
        event.preventDefault();

        window.ref = $('li.active a').data('numero');
        window.adicionar_acao = 'diario';
        window.data = $('#diario-datas').val();

        $.get('template/adicionar-alimento.html', function (data) {
            bootbox.alert({
                message: data,
                title: '<h4 id="alm-nome"></h4>'
            });
        });
    });

    $('#my-tab-content').on('click','a[rel="facebox"]', function () {
        window.editar_acao = 'diario';
        window.ref = $(this).parent().parent().data('ref');
        window.alm_uid = $(this).parent().parent().data('uid');
        window.alm_qnt = $(this).parent().parent().data('quantidade');
        window.dataDiario = $('#diario-datas').val();
        /*jQuery.facebox({ ajax: 'template/editar-alimento.html' });*/
        $.get('template/editar-alimento.html', function (data) {
            bootbox.alert({
                message: data,
                title: '<h4 id="alm-nome"></h4>'
            });
        });
    });

});

$.carregarDiario = function(){
    window.totais_diario = {
        carboidratos: 0,
        proteinas: 0,
        gorduras: 0,
        total:0
    };

    var d          = new Date();
    var month      = d.getMonth()+1;
    var day        = d.getDate();
    var data       = (day<10 ? '0' : '') + day + '/' + (month<10 ? '0' : '') + month + '/' + d.getFullYear();
    var diarioData = "";

    if($('#diario-datas').val().length == 0){
        diarioData = data;
        $('#diario-datas').append('<option value="'+data+'" selected>'+data+'</option>');
    } else {
        diarioData = $('#diario-datas').val();
    }

    var data_enviar =
        'uid='      + $.cookie('uid')                 +
        '&email='   + $.cookie('email')               +
        '&data='    + diarioData;

    $.ajax({
        type: 'post',
        data: data_enviar,
        url: window.link+'api/diario/act-get-diario.php',
        success: function (retorno) {
            var data = retorno.return;
            console.log(retorno);
            $('#data-diario').html(data.data);
            if('refeicao' in data){
                if(data.refeicao.length > 1){
                    for(n=2;n <= data.refeicao.length;n++){
                        if($('#li-ref'+ n).length == 0){
                            $('#add-refeicao').before(
                                '<li id="li-ref'+ n +'">' +
                                    '<a href="#ref-'+ n +'" data-ir="ref-'+ n +'" data-numero="'+ n +'" data-toggle="tab">' +
                                        'Refeição '+n+'' +
                                    '</a>' +
                                '</li>'
                            );
                        }
                        if($('#ref-'+n).length == 0){
                            $('#my-tab-content').append(
                                '<div class="tab-pane" id="ref-'+ n +'">'+
                                    '<div id="diary-table" ><table class="table table-responsive">'+
                                        '<thead>'+
                                            '<th>Alimento</th>'+
                                            '<th style="text-align: center">Quantidade</th>' +
                                            '<th style="text-align: center">Calorias</th>'+
                                            '<th style="text-align: center">Editar</th>'+
                                            '<th style="text-align: center">Remover</th>'+
                                        '</thead>'+
                                        '<tbody>'+
                                        '</tbody>'+
                                    '</table></div>'+
                                '</div>'
                            );
                        }
                    }
                }
                $.each(data.refeicao, function (a,b) {
                    a++;
                    $('#ref-'+ a + ' tbody').html('');
                    $.each(b, function (c,d) {
                        var alimento = {
                            gramas: {
                                carboidratos: d.carboidratos,
                                proteinas: d.proteinas,
                                gorduras: d.gorduras
                            },
                            calorias: {
                                carboidratos: (d.carboidratos * 4),
                                proteinas: (d.proteinas * 4),
                                gorduras: (d.gorduras * 9),
                                total: parseInt(((d.carboidratos * 4) + (d.proteinas * 4) + (d.gorduras * 9)))
                            }
                        };

                        $('#ref-'+ a + ' tbody').append(
                            '<tr data-ref="'+ a +'" data-quantidade="'+ d.quantidade +'" data-uid="'+ d.uid +'" data-carboidratos="'+ alimento.gramas.carboidratos +'" data-proteinas="'+ alimento.gramas.proteinas +'" data-gorduras="'+ alimento.gramas.gorduras +'">' +
                                '<td>'+ d.nome +'</td>' +
                                '<td style="text-align: center">'+ d.quantidade +' g</td>' +
                                '<td style="text-align: center">'+ alimento.calorias.total +' kcal</td>' +
                                '<td style="text-align: center"><a rel="facebox" href="#"><i class="fa fa-exchange"></i></a></td>' +
                                '<td class="remover-alimento" style="text-align: center"><i class="fa fa-times" style="color: darkred;cursor: pointer"></i></td>' +
                            '</tr>'
                        );

                        window.totais_diario.carboidratos += alimento.calorias.carboidratos;
                        window.totais_diario.gorduras     += alimento.calorias.gorduras;
                        window.totais_diario.proteinas    += alimento.calorias.proteinas;
                        window.totais_diario.total        += alimento.calorias.total;
                    });
                });
                $.getjson();
            }else{
                $('#tabs').html(
                    '<li class="active">'+
                    '   <a href="#ref-1" data-ir="ref-1" data-numero="1" data-toggle="tab">Refeição 1</a>'+
                '</li>'+
                '<li id="add-refeicao"><a href="#">+</a></li>'+
                '   <li id="menos-refeicao" style="display: none">'+
                '<a href="#">-</a>'+
                    '</li>');
                $('#my-tab-content').html(
                    '<div class="tab-pane active" id="ref-1">'+
                    '   <div id="diary-table">'+
                '   <table id="table-diario-alimentos" class="table table-responsive table-striped">'+
                '   <thead>'+
                '   <th>Alimento</th>'+
                '   <th style="text-align: center">Quantidade</th>'+
                '   <th style="text-align: center">Calorias</th>'+
                '   <th style="text-align: center">Editar</th>'+
                '   <th style="text-align: center">Remover</th>'+
                '   </thead>'+
                '   <tbody>'+
                '   </tbody>'+
                '   </table>'+
                '   </div>'+
                '</div>'
                );
                window.totais_diario.carboidratos = 0;
                window.totais_diario.gorduras     = 0;
                window.totais_diario.proteinas    = 0;
                window.totais_diario.total        = 0;
            }
            $('#diario-div,#diario-grafico').fadeIn('slow');
            $.getjson();
        }
    });

};
$.getjson = function(){
    $.getJSON( window.userjson, function( data ) {
        window.json = data;
        console.log(data);
        JsonRequired();

        if('dieta' in window.json) {
            var dieta = window.json.dieta[(window.json.dieta.length)-1];
            var dieta_meta = {
                macros : {
                    gramas :{
                        gorduras     : (dieta.SomaKcal * (dieta.gorduras     / 100)) / 9 ,
                        proteinas    : (dieta.SomaKcal * (dieta.proteinas    / 100)) / 4 ,
                        carboidratos : (dieta.SomaKcal * (dieta.carboidratos / 100)) / 4
                    },
                    calorias :{
                        gorduras     : dieta.SomaKcal * (dieta.gorduras     / 100),
                        proteinas    : dieta.SomaKcal * (dieta.proteinas    / 100),
                        carboidratos : dieta.SomaKcal * (dieta.carboidratos / 100),
                        total : dieta.SomaKcal
                    }
                }
            };

            if(window.totais_diario.total > 0){ $('#chartDiario').fadeIn('slow'); }

            var seriesChart = {
                proteinas:0,
                carboidratos:0,
                gorduras:0,
                total:0
            };
            if((dieta_meta.macros.calorias.proteinas-window.totais_diario.proteinas) > 0){
                seriesChart.proteinas = (dieta_meta.macros.calorias.proteinas-window.totais_diario.proteinas);
            }
            if((dieta_meta.macros.calorias.carboidratos-window.totais_diario.carboidratos) > 0){
                seriesChart.carboidratos = (dieta_meta.macros.calorias.carboidratos-window.totais_diario.carboidratos);
            }
            if((dieta_meta.macros.calorias.gorduras-window.totais_diario.gorduras) > 0){
                seriesChart.gorduras = (dieta_meta.macros.calorias.gorduras-window.totais_diario.gorduras);
            }
            if((dieta_meta.macros.calorias.total-window.totais_diario.total) > 0){
                seriesChart.total = (dieta_meta.macros.calorias.total-window.totais_diario.total);
            }

            new Chartist.Bar('#chartDiario', {
                labels: ['Proteina', 'Carboidratos', 'Gorduras', 'Total'],
                series: [
                    [window.totais_diario.proteinas, window.totais_diario.carboidratos, window.totais_diario.gorduras, window.totais_diario.total],
                    [seriesChart.proteinas,seriesChart.carboidratos,seriesChart.gorduras,seriesChart.total]
                ]
            }, {
                stackBars: true
            }).on('draw', function(data) {
                    if(data.type === 'bar') {
                        data.element.attr({
                            style: 'stroke-width: 30px'
                        });
                    }
                });
        } else {

            if(window.totais_diario.total > 0){ $('#chartDiario').fadeIn('slow');}

            var dieta_meta = {
                macros:{
                    calorias:{
                        carboidratos:0,
                        proteinas: 0,
                        gorduras: 0,
                        total:0
                    }
                }
            };

            new Chartist.Bar('#chartDiario', {
                labels: ['Proteina', 'Carboidratos', 'Gorduras', 'Calorias'],
                series: [
                    [window.totais_diario.proteinas, window.totais_diario.carboidratos, window.totais_diario.gorduras, window.totais_diario.total],
                    [0, 0, 0, 0]
                ]
            }, {
                stackBars: true
            }).on('draw', function(data) {
                    if(data.type === 'bar') {
                        data.element.attr({
                            style: 'stroke-width: 30px'
                        });
                    }
                });
        }

        $('#data-grafico-carboidratos').html(
            '<td>Carboidratos</td>'+
            '<td style="text-align: center">'+ parseInt((window.totais_diario.carboidratos / 4)) +' g</td>'+
            '<td style="text-align: center">'+
                '<font style="color:darkred;">' + parseInt(window.totais_diario.carboidratos) +' kcal</font>'+
            '</td><td style="text-align: center">'+
                '<font style="color:rgb(255, 74, 85);">' + parseInt(dieta_meta.macros.calorias.carboidratos) +' kcal</font></td>'
        );

        $('#data-grafico-proteinas').html(
            '<td>Proteinas</td>'+
            '<td style="text-align: center">'+ parseInt((window.totais_diario.proteinas / 4)) +' g</td>'+
            '<td style="text-align: center">'+
                '<font style="color:darkred;">' + parseInt(window.totais_diario.proteinas) +' kcal</font>' +
            '</td><td style="text-align: center">'+
                '<font style="color:rgb(255, 74, 85);">' + parseInt(dieta_meta.macros.calorias.proteinas) +' kcal</font></td>'
        );

        $('#data-grafico-gorduras').html(
            '<td>Gorduras</td>'+
            '<td style="text-align: center">'+ parseInt((window.totais_diario.gorduras / 9)) +' g</td>'+
            '<td style="text-align: center">'+
                '<font style="color:darkred;">'+parseInt(window.totais_diario.gorduras) +' kcal</font>'+
            '</td><td style="text-align: center">'+
                '<font style="color:rgb(255, 74, 85);">'+parseInt(dieta_meta.macros.calorias.gorduras) +' kcal</font></td>'
        );

        $('#data-grafico-total').html(
            '<td>Total</td>' +
            '<td style="text-align: center">'+ parseInt((window.totais_diario.carboidratos / 4) + (window.totais_diario.proteinas / 4) + (window.totais_diario.gorduras / 9)) +' g</td>'+
            '<td style="text-align: center">'+
                '<font style="color:darkred;">'+parseInt(window.totais_diario.total) +' kcal</font>'+
            '</td><td style="text-align: center">'+
                '<font style="color:rgb(255, 74, 85);">'+parseInt(dieta_meta.macros.calorias.total) +' kcal</font>' +
            '</td>'
        );
        $('#copiarRefeicao').fadeIn('slow');
    }).fail(function() {
        $.removeCookie("nome");
        $.removeCookie("email");
        $.removeCookie("uid");
        window.location.replace("index.html");
    });
};

function addRefeicao(){
    $('#add-refeicao').click(function () {
        var n = ($('#tabs li').length-1);

        if(n >= 2){ $('#menos-refeicao').fadeIn('slow'); } else { $('#menos-refeicao').fadeOut('slow'); }

        $(this).before(
            '<li id="li-ref'+ n +'">'                                                                    +
                '<a href="#ref-'+ n +'" data-ir="ref-'+ n +'" data-numero="'+ n +'" data-toggle="tab">'  +
                    'Refeição '+ n                                                                       +
                '</a>'                                                                                   +
            '</li>'
        );

        $('#my-tab-content').append('' +
            '<div class="tab-pane" id="ref-'+ n +'">'                       +
                '<table class="table table-responsive">'     +
                    '<thead>'                                               +
                        '<th>Alimento</th>'                                 +
                        '<th>Quantidade</th>'                               +
                        '<th>Calorias</th>'                                 +
                        '<th>Editar</th>'                                   +
                        '<th>Excluir</th>'                                  +
                    '</thead>'                                              +
                    '<tbody>'                                               +
                    '</tbody>'                                              +
                '</table>'                                                  +
            '</div>'
        );
    });
}
function removerRefeicao(){
    $('#menos-refeicao').click(function () {
        var n = ($('#tabs li').length-2);

        var data_enviar =
            'uid='    + $.cookie('uid')                 +
            '&ref='   + n +
            '&data='  + $('#diario-datas').val() +
            '&email=' + $.cookie('email');

        $.ajax({
            type: 'post',
            data: data_enviar,
            url: window.link + 'api/diario/act-apagar-refeicao.php',
            success: function (retorno) {
                console.log(retorno);
                if(retorno.reply.code ==1){
                    $('#ref-'+ n).remove();
                    $('#li-ref'+ n).remove();
                    if(n >= 3){ $('#menos-refeicao').fadeIn('slow'); } else { $('#menos-refeicao').fadeOut('slow'); }
                }
            }
        });

    });
}
function removerAlimento(){
    $('#diario-div').on('click','.remover-alimento', function () {
        var alm = $(this).parent().data('uid');
        var ref = $(this).parent().data('ref');
        var data_enviar =
            'uid='      + $.cookie('uid')                 +
            '&ref='+ ref +
            '&alm='+ alm+
            '&data='+ $('#diario-datas').val() +
            '&email='   + $.cookie('email');
        bootbox.confirm({ 
            size: 'small',
            message: 'Tem certeza que deseja excluir o alimento do Diário?', 
            callback: function(result){
                if (result) {
                    $.ajax({
                        type: 'post',
                        data: data_enviar,
                        url: window.link + 'api/diario/act-apagar-alimento.php',
                        success: function (retorno) {
                            if(retorno.reply.code ==1){
                                $('#diario-div,#diario-grafico').fadeOut('slow');
                                $.getjson();
                                $.carregarDiario();
                            }
                        }
                    });
                };
            }
        })

    })
}
function diarioSelectChange(){
    $('#diario-datas').on('change', function () {
        $('#diario-div,#diario-grafico').fadeOut('slow');
        $.carregarDiario();
    })
}
function copiarDieta(){
    var ref = $('#tabs > li.active').children('a').data('numero');
    var data_enviar =
        'uid='      + $.cookie('uid')                           +
        '&email='   + $.cookie('email')                         +
        '&ref='     + ref                                       +
        '&data='    + $('#diario-datas').val();
    $.ajax({
        type: 'post',
        data: data_enviar,
        url: window.link+'api/diario/act-copiar-dieta.php',
        success: function (retorno) {
            if(retorno.reply.code == 1){
                $('#diario-div,#diario-grafico').fadeOut('slow');
                $.carregarDiario();
            }
        }
    });
}