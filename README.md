![Captura de Tela 2016-01-01 às 16.51.36.png](https://bitbucket.org/repo/a6rGno/images/3071855518-Captura%20de%20Tela%202016-01-01%20%C3%A0s%2016.51.36.png)

# README #

Olá,

Bem vindo ao desafio do Anilha.

Por chegar até aqui, provavelmente você é um dos pré-selecionados à vaga de desenvolvedor front-end.

Faça um fork deste repositório (privado), adicionando @samuelfaj à ele.

Sua tarefa é: **utilizando se da framework que está sendo estruturada em js-global.js do branch dev, você deve estruturar todo front end dessa maneira. Como ocorre em index.js e perfil.js. Você deverá também testar tudo e consertar todos os possíveis bugs que aparecerem.**

### Branches ###

* **master** - Para aplicação testada e funcionando.
* **dev** - Para alteraçōes e desenvolvimento.

** Você pode criar quantos branchs desejar, porém o único que deve conter o seu trabalho finalizado é o Master. Você não deve commitar no master sem o trabalho estar finalizado. **

### Documentação ###

* **Sempre commitar muito bem, explicar exatamente o que foi feito.**

### Avaliação ###
Irá contar para a avaliação:

* **Prazo** - Será levado em consideração a execução deste desafio no prazo estipulado por você.
* **Capricho** - Será avaliado o código em si. Deve ser muito bem legível, de fácil manutenção e estruturado.
* **Inovação** - Contará muitos pontos caso você encontre um jeito melhor de fazer as coisas ou inove de alguma forma na proposta dada à você.

**Você não deve usar dupla barra em comentários do código **

** Certo: **

```
#!javascript

console.log('Esse é um teste') /* Comentando para avisar que este é um teste. */
```

** Errado: **

```
#!javascript

console.log('Esse é um teste') // Comentando para avisar que este é um teste.
```